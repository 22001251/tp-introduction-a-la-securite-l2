#include <stdio.h>
#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

//Renvoie une liste vide
list_t list_empty(){
  return NULL;
}

//Teste si la liste est vide
int list_is_empty(list_t l){
  return l == NULL;
}

//Ajoute et créé une nouvelle case
list_t list_push(list_t l, void* x){
  list_t new = malloc(sizeof(struct cell_t));
  new -> val = x;
  if(list_is_empty(l)){
    new -> id = 1;
  }
  else{
    new -> id = l -> id + 1;
  }
  new -> next = l;
  return new;
}

//Renvoie la queue de la liste
list_t list_tail(list_t l){
  return l -> next;
}

//Renvoie la première valeur
void* list_pop(list_t* l){
  list_t tmp = (*l) -> next;
  void* res = (*l) -> val;
  free(*l);
  *l = tmp;
  return res;
}

//Renvoie la première valeur dans la première case
void* list_top(list_t l){
  return l -> val;
}

//Free sur toutes les cases et toutes les valeurs
void list_destroy(list_t l, void (*free_void)(void*)){
  while(!list_is_empty(l)){
    free_void(list_pop(&l));
  }
}

//Renvoie l'élément trouvé ou NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  while(l != NULL){
    if(eq(l -> val, x)){
      return l -> val;
    }
    l = list_tail(l);
  }
  return NULL;
}

//Renvoie la longueur de la liste
unsigned long int list_len(list_t l){
  return l -> id;
}
